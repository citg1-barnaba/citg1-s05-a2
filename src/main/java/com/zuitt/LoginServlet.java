package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;


public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8286177122798244808L;
	
	public void init() throws ServletException{
		System.out.println("LoginServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		
		HttpSession session = req.getSession();
		
		session.setAttribute("firstname", firstname);
		session.setAttribute("lastname", lastname);
		
		res.sendRedirect("home.jsp");
	}
	
	public void destroy(){
		System.out.println("LoginServlet has been finalized.");
	}

}
