package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;
	
	public void init() throws ServletException{
		System.out.println("RegisterServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String discovery = req.getParameter("discovery");
		String dateOfBirth = req.getParameter("date_of_birth");
		String choices = req.getParameter("choices");
		String description = req.getParameter("description");
		
		HttpSession session = req.getSession();
		
		session.setAttribute("firstname", firstname);
		session.setAttribute("lastname", lastname);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("discovery", discovery);
		session.setAttribute("date_of_birth", dateOfBirth);
		session.setAttribute("choices", choices);
		session.setAttribute("description", description);
		
		res.sendRedirect("register.jsp");
	}
	
	public void destroy() {
		System.out.println("RegisterServlet has been finalized.");
	}

}
