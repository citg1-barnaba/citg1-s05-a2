<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Register Confirmation</title>
	</head>
	<body>
		<%
			String discovery = session.getAttribute("discovery").toString();
			if(discovery.equals("friends")){
				discovery = "Friends";
			}
			else if(discovery.equals("socmed")){
				discovery = "Social Media";
			}
			else{
				discovery = "Others";
			}
		%>
		<h1>Registration Confirmation</h1>
		<p>First Name: <%= session.getAttribute("firstname") %></p>
		<p>Last Name: <%= session.getAttribute("lastname") %></p>
		<p>Phone: <%= session.getAttribute("phone") %></p>
		<p>Email: <%= session.getAttribute("email") %></p>
		<p>App Discovery: <%= discovery %></p>
		<p>Date of birth: <%= session.getAttribute("date_of_birth") %></p>
		<p>User Type: <%= session.getAttribute("choices") %></p>
		<p>Description: <%= session.getAttribute("description") %></p>
		
		<form action="home.jsp" method="post">
			<input type="submit">
		</form>
		
		<form action="index.jsp" >
			<input type="submit" value="Back">
		</form>
		
	</body>
</html>