<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Welcome</title>
	</head>
	<body>
		<h1>Welcome <%= session.getAttribute("firstname") %> <%= session.getAttribute("lastname") %></h1>
		<%
			if(session.getAttribute("choices").equals("applicant")){
				out.println("Welcome Applicant. You may now start looking for your career opportunity.");
			}
			if(session.getAttribute("choices").equals("employer")){
				out.println("Welcome Employer. You may now start browsing applicant profiles");
			}
		%>
	</body>
</html>