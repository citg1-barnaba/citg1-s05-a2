<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Job Finder App</title>
		<style>
			div{
				margin-top: 5px;
				margin-bottom: 5px;
			}
		</style>
	</head>
	<body>
		<div id="container">
			<h1>Welcome to Servlet Job Finder!</h1>
			<form action="register" method="post">
				<div>
					<label for="firstname">First Name </label>
					<input type="text" name="firstname">
				</div>
				<div>
					<label for="lastname">Last Name </label>
					<input type="text" name="lastname">
				</div>
				<div>
					<label for="phone">Phone </label>
					<input type="tel" name="phone">
				</div>
				<div>
					<label for="email">Email </label>
					<input type="email" name="email">
				</div>
				<fieldset>
					<legend>How did you discover the app?</legend>
					
					<input type="radio" id="friends" name="discovery" value="friends" required>
					<label for="friends">Friends</label>
					<input type="radio" id="socmed" name="discovery" value="socmed" required>
					<label for="socmed">Social Media</label>
					<input type="radio" id="others" name="discovery" value="others" required>
					<label for="others">Others</label>
				</fieldset>
				<div>
					<label for ="date_of_birth">Date of birth</label>
					<input type="date" name="date_of_birth" required>
				</div>
				<div>
					<label for ="choices">Are you an employer or applicant?</label>
					<select id="choice_type" name="choices">
						<option value="" selected="selected">Select One</option>
						<option value="applicant">Applicant</option>
						<option value="employer">Employer</option>
					</select>
				</div>
				<div>
					<label for="description">Profile Description</label>
					<textarea name="description" maxlength="500"></textarea>
				</div>
				<button>Register</button>
			</form>
		</div>
	</body>
</html>